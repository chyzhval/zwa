<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Form\CommentType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class CommentController extends Controller
{

    /**
     * @Template()
     * @Route("/doctor/{forDoctor}/", name="doctor_index")
     */
    public function indexAction(Request $request, $forDoctor)
    {
        $comments = $this->getDoctrine()
            ->getRepository(Comment::class)
            ->findBy([ 'forDoctor' => $forDoctor ]);

        $dql = "SELECT c FROM AppBundle:Comment c";
        // $query = $this->getDoctrine()
        //         ->getRepository(Comment::class)
        //         ->createQuery($query);    
        /**
         * @var $paginator \Knp\Component\Pager\Paginator
         */
        $paginator = $this->get("knp_paginator");
        $result = $paginator->paginate(
            $comments,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 6)
        );

        $comment = new Comment($forDoctor);
        $form = $this->createForm(CommentType::class, $comment)
            ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->persist($comment);
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('doctor_index', [ 'forDoctor' => $forDoctor ]);
        }

        return [
            'comments' => $result,
            'form' => $form->createView()
        ];
    }

}
